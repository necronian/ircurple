(ns ircurple.core
  (:require [ircurple.connection :as c]
            [ircurple.middleware :as m]))

(defn join
  [room]
  (c/send-irc-message {:command "JOIN" :params room}))

(defn hello-middleware
  [handler]
  (fn [message]
    (when (= "PRIVMSG" (:command message))
      (when (re-matches #"(?i)^hello.*purplebot.*" (:text message))
        (c/send-irc-message (:connection message)
                            {:command "PRIVMSG"
                             :params [(:target message)
                                      (str "Hello, " (get-in message [:prefix :nickname]))]})))
    (handler message)))

(defn test-handler
  [message]
  message)

(def app
  (-> test-handler
      hello-middleware
      m/prettify-irc
      m/pong
      m/raw-log))

(defn -main
  [{:keys [nick user pass name host handler port ssl?]
    :or {nick "ircurple" user "ircurple" name "ircurple" ssl? false}}]
  (println nick)
  (let [connection (c/start-irc handler host port)]
    (c/default-connection connection)

    (when pass
      (c/send-irc-message connection {:command "PASS" :params pass}))

    (c/send-irc-message connection {:command "NICK" :params nick})
    (c/send-irc-message connection {:command "USER" :params [nick "12" "*" name]})
    
    connection))
