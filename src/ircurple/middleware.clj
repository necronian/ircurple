(ns ircurple.middleware
  (:require [ircurple.connection :as c]))

(defn incoming?
  "Test if this is a message originating from the irc server."
  [message]
  (= :incoming (:source message)))

(defn raw-log
  "This middleware is for debugging and just prints incoming/outgoing messages to
   STDOUT."
  [handler]
  (fn [message]
    (condp = (:source message)
      :incoming (println "<< " (:raw message))
      :outgoing (println ">> " (:raw message)))
    (handler message)))

(defn pong
  "The most important middleware, sends pong messages in response to server
   ping messages. Keeps the client from timing out on the server."
  [handler]
  (fn [message]
    (when (and  (incoming? message)
                (= "PING" (:command message)))
      (c/send-irc-message (:connection message)
                          {:command "PONG"
                           :params (:params message)}))
    (handler message)))

(defn prettify-irc
  "Make the parsed irc messages slightly nicer for users consuming the api.
   Further break out parameters based on message types."
  [handler]
  (fn [message]
    (->
     (condp = (:command message)
       "PRIVMSG" (merge message
                        {:target (first (:params message))
                         :text (second (:params message))})
       message)
     (handler))))
