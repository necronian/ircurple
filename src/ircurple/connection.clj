(ns ircurple.connection
  (:require [instaparse.core :as insta]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.core.async :as async :refer [go-loop >! >!! <! chan close! sliding-buffer]])
  (:import java.net.Socket
           javax.net.ssl.SSLSocketFactory
           java.io.IOException))

(def ^{:private true} _default (atom {}))

;;; Mostly copy pasted from rfc 2812.
(def ^{:private true} irc-message-parser
  (instaparse.core/parser
   (clojure.java.io/resource "irc.abnf")
   :input-format :abnf))

(defn- parse-message
  "Takes an irc message string and parses it."
  [s]
  (->> (irc-message-parser s)
       (insta/transform {:hostname str
                         :nickname (fn [coll] {:nickname coll})
                         :user (fn [coll] {:user coll})
                         :host (fn [coll] {:host coll})
                         :servername (fn [coll] {:servername coll})
                         :prefix (fn [& coll] {:prefix (apply merge coll)})
                         :command (fn [coll] {:command coll})
                         :params (fn [& coll] {:params (into '[] coll)})
                         :message (fn [& coll] (apply merge coll))})))

(defn- encode-params
  "Takes a vector of parameters and collects them into a valid irc parameter string."
  [coll]
  (if-not (sequential? coll)
    (str " " coll)
    (if (= 1 (count coll))
      (str " " (first coll))
      (->> (into (pop coll) [(str ":" (last coll))])
           (interpose " ")
           (apply str)
           (str " ")))))

(defn- encode-message
  "Takes an input collection in the same format as a parsed irc message 
   and converts it to an irc message string."
  [coll]
  (let [prefix (when (:prefix coll) (str ":" (:prefix coll) " "))
        command (:command coll)
        params (:params coll)]
    (str prefix command (encode-params params) "\r\n")))


(defn default-connection
  "Allows the api use to set a default connection so that they are 
   not required to pass the connection map to every function they call."
  [coll]
  (reset! _default coll))

(defn send-irc-message
  "Takes a connection map and an irc message map encodes the message to a raw irc string and writes it
   to the connection socket. If no connection map is supplied it will use the default connection value."
  ([coll] (send-irc-message @_default coll))
  ([connection coll]
   (let [message (merge {:source :outgoing
                         :connection connection
                         :raw (encode-message coll)
                         :parsed coll}
                        coll)]
     (>!! (:out connection) (encode-message ((:handler connection) message))))))

(defn stop-irc
  "Close the socket and close the async channels"
  [connection]
  (println "CLEANUP STUFF NOW?")
  (.close (:socket connection))
  (close! (:in connection))
  (close! (:out connection)))

(defn setup-socket
  "Open a socket to host and port and redirect i/o to core.async channels then return a map
   containing the socket and in and out channels."
  [host port & {:keys [ssl? in-buffer out-buffer]
                :or {:ssl? false
                     :in-buffer (sliding-buffer 100)
                     :out-buffer (sliding-buffer 100)}}]
  (let [socket (if ssl?
                 (.createSocket (SSLSocketFactory/getDefault) host port)
                 (Socket. host port))
        in (io/reader socket)
        in-ch  (chan in-buffer)
        out-ch (chan out-buffer)
        connection {:in in-ch :out out-ch :socket socket}]

    ;;; Setup read from socket
    (go-loop []
      (let [line (try (.readLine in)
                      (catch IOException _ nil))]
        (if-not line
          (stop-irc connection)
          (do (>! in-ch line)
              (recur)))))

    ;;; Setup write to socket
    (go-loop []
      (let [line (<! out-ch)]
        (if (.isClosed socket)
          (stop-irc connection)
          (let [out (io/writer socket)]
            (.write out line)
            (.flush out)
            (recur)))))
    
    connection))

(defn start-irc
  "Starts an irc socket and then runs handler on every irc message received."
  [handler host port & {:keys [ssl?] :or {:ssl? false}}]
  (let [connection (merge {:handler handler
                           :host host
                           :port port
                           :ssl? ssl?}
                          (setup-socket host port))]
    
    (go-loop []
      (let [message (<! (:in connection))]
        (if-not message
          (stop-irc connection)
          (let [parsed (parse-message message)
                message (merge {:connection connection
                                :raw message
                                :parsed parsed
                                :source :incoming}
                               parsed)]
            ((:handler connection) message)
            (recur)))))
    
    connection))
