(set-env!
 :project 'ircurple
 :version "0.1.0-SNAPSHOT"
 :source-paths #{"src" "test"}
 :resource-paths #{"resources"}
 :dependencies '[[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "0.2.374"]
                 [instaparse "1.4.1"]
                 [adzerk/boot-test "1.1.1" :scope "test"]])

(require '[adzerk.boot-test :refer :all])

(task-options!
 pom {:project (get-env :project)
      :version (get-env :version)}
 aot {:namespace '#{ircurple.core}}
 jar {:main 'ircurple.core}
 repl {:init-ns 'user
       :skip-init true})

(replace-task!
 [r repl] (fn [& xs]
            (merge-env! :source-paths #{"dev"})
            (apply r xs)))

(deftask build
  "Build a JAR file"
  []
  (comp
   (aot)
   (pom)
   (jar)))
